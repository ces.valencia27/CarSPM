import XCTest

import CarSPMTests

var tests = [XCTestCaseEntry]()
tests += CarSPMTests.allTests()
XCTMain(tests)
